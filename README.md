# Kelkaj fontoj por krei kartarojn por Anki.

Ĉi tie mi aldonas fontojn (CVS dosierojn kaj bildojn) por kelkaj kartaroj por lerni Esperanton.

## Kiel krei Anki kartaro el bildo-dosieroj?

1. Krei CSV dosiero por Anki el listo de PNG dosieroj kies baznomo estos la dorso flanko de la Anki-kartoj.
```find . -iname "*.png" -type f  -exec echo '{}' \| \<img src=\"'{}'\"/\> \; | sed -e "s/.png//" -e "s/\.\///g" > BildokartaroPorAnki.csv``` 

2. Kopiu la PNG dosieroj al la loka dosierujo de Anki:
```cp *.png ~/.var/app/net.ankiweb.Anki/data/Anki2/1-a\ uzanto/collection.media/```
(ĉi tiu celloko povas esti malsama depende de la Anki-uzantnomo, Anki-versio kaj operaciumo.)

3. Enportu la dosieron ```BildokartaroPorAnki.csv``` per Anki. 

## Permesilo

Ĉiu tie ĉi havas permesilon CC0, publika havaĵo. Do vi povas uzi ilin tute libere.

## Fontolisto

Fonto de la bildoj: https://commons.wikimedia.org (CC0, publika havaĵo)

